#!/usr/bin/env bash
###
# Wrapper file to start the Arma3 server
#
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
# https://creativecommons.org/licenses/by-nc/4.0/
#
# @version    2020.01.26
# @author     ©TriMoon™ <https://gitlab.com/TriMoon>
# @copyright  (C) 2019+ {@link https://gitlab.com/TriMoon|©TriMoon™}
# @license    CC-BY-NC-4.0
###

# Make sure we run as "<UNIX USER>"
if test $(id -u) -eq 0; then
	printf "Switching to user '<UNIX USER>'...\n"
	exec runuser -u <UNIX USER> -- $0 "$@"
elif test $(id -u) -ne $(id -u <UNIX USER>); then
	printf "Error: This script needs to be run as user '<UNIX USER>' !\n" >&2
	# systemd.exec(5): EXIT_NOPERMISSION
	exit 4
fi

# Make sure the rundir is present to put our pid in.
if test ! -d "<RUN DIR>"; then
	printf "'<RUN DIR>' is not available !\n" >&2
	# systemd.exec(5): EXIT_RUNTIME_DIRECTORY
	exit 233
fi

# Start server.
cd ~/SERVER/"<INSTANCE>"
printf "Starting server...\n"
# Using "-a name" so it looks better in process listing of ps.
exec -a arma3server \
	./arma3server \
	-par=serverconfig/<PARAM FILE> "$@"
