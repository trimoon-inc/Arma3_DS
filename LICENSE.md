![License logo]\
Unless otherwise explicitly noted:\
All work in this repo is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License][License link].

[License logo]: https://i.creativecommons.org/l/by-nc/4.0/88x31.png "Creative Commons License"
[License link]: http://creativecommons.org/licenses/by-nc/4.0/

# Exceptions:
1. `/images`:\
Images taken from the internet were taken as "Fair use".\
_(They can be easily found if you look for them)_
   - `Bohemia_Interactive.svg` was taken from the official website's html and base64 decoded.
   - `Bohemia_Interactive_black.svg` is copy of the above with manual change of fill-color by me.
   - `arma3.png` was taken from the official website.
   - `arma3_black.png` is copy of the above with manual change of fill-color by me.
   - `arma3_logo.png` was taken from (need to backtrace the source i got it from)
   - `arma3_logo_icon.png` is copy of the above with automated conversion of size using `convert` by me.
