[![Company logo]][Company site]\
[![Game logo]][Game site]\
Linux Dedicated Game Server

This is a base setup for an _**Arma 3** Linux Dedicated Game Server<sup>(LDGS)</sup>_ available via [Steam][Steam page].

---
# <abbr title="Table of Contents">TOC</abbr>:<!-- omit in toc -->
1. [Install](#install)
2. [Important files and scripts](#important-files-and-scripts)
   1. [Files](#files)
      1. [arma3ds](#arma3ds)
      2. [parameters.cfg](#parameterscfg)
      3. [basic.cfg](#basiccfg)
      4. [server.cfg](#servercfg)
      5. [server.Arma3Profile](#serverarma3profile)
      6. [servervars.Arma3Profile](#servervarsarma3profile)
      7. [kick.txt & ban.txt](#kicktxt--bantxt)
   2. [Scripts](#scripts)
      1. [updateDS](#updateds)
      2. [createInstance](#createinstance)
      3. [startserver](#startserver)
      4. [startAndTailInstance](#startandtailinstance)
3. [References](#references)
4. [Footnotes](#footnotes)

# Install
In the steps below replace:\
`<BASE>` with the full-path you want to install into.
1. Install the dependencies needed for SteamCMD if they are not present yet.\
   See: [ValveWiki: SteamCMD#Manually][SteamCMD Info] and [ValveWiki: 32bit libs on 64bit][SteamCMD 32bit libs on 64bit]\
   In a nutshell:
   1. Enable the 32-bit Architecture, if not already done so for other software.
   2. Install the `lib32stdc++6` package, if not already done so for other software.
2. Clone into a directory of choice.\
   This directory will be used as the home directory of the user that will be used to run the server as.\
   This directory does not have to exist yet, but if it does it needs to be empty otherwise git can not clone into it.\
  `git clone https://gitlab.com/trimoon-inc/Arma3_DS.git "<BASE>"`
3. Run the `1stTimeInit` script to prepare the dedicated server.\
  `cd "<BASE>"`\
  `sudo bin/1stTimeInit`
4. **Follow the instruction shown on screen**...\
   After this step **only** the account(s) that will act as administrator of the _Linux Dedicated Game Server_ will have access to the installed directory.
   _(besides the user the server will run as)_\
   So make **sure** the account(s), that will act as administrator of the <abbr title="Linux Dedicated Game Server">LDGS</abbr>, are part of the `admin_group` as set in the [arma3ds config file](#arma3ds) or they **won't** be able to edit nor access any of the files !

<details>
<summary>Examples:</summary>

See **step 1** of _[the Install instructions](#install)_:
```console
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install lib32stdc++6
```
See **step 2** of _[the Install instructions](#install)_:
```console
git clone https://gitlab.com/trimoon-inc/Arma3_DS.git "/opt/Arma3_DS"
cd "/opt/Arma3_DS"
sudo bin/1stTimeInit
```
</details>

# Important files and scripts

## Files

### arma3ds
This is the **system-wide** configuration file to store important settings used by all scripts for the <abbr title="Linux Dedicated Game Server">LDGS</abbr>.
- Located at: `/etc/default/arma3ds`
- In this file you can set values for:
  1. `unix_user`\
   This is the unix username the server will run as.
  2. `admin_group`\
   This is the unix groupname the admins of the <abbr title="Linux Dedicated Game Server">LDGS</abbr> need to belong to.\
   Default template uses: `adm`
  3. `steam_account` and `steam_password`\
   These are the steam credentials used to install/update the server files.\
   You can add various extra info in the comments above it for useful accounting info.
  4. `admin_uuids`\
   This is a _bash-array_ of UUID's that will be granted admin status using `#login` in the game chat without requiring a password.\
   Take note that this _bash-array_ uses a seperate line per entry for clarity, because UUID's are numbers and thus have no space in them...

### parameters.cfg
This file holds command line arguments, one per line, used to start the server with.\
See [BiWiki: Arma 3 Startup Parameters][Arma 3 Startup Parameters] for more info.
- Located at: `SERVER/<instance name>/serverconfig/parameters.cfg`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._

### basic.cfg
In this configuration file you should configure your serverinstance's connectivity, mainly for performance tuning.\
See [BiWiki: basic.cfg][basic cfg] for more info.
- Located at: `SERVER/<instance name>/serverconfig/basic.cfg`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._

### server.cfg
This is the **main configuration** file for the server instance.\
See [BiWiki: server.cfg][server cfg] for more info.
- Located at: `SERVER/<instance name>/serverconfig/server.cfg`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._

### server.Arma3Profile
This configuration file describes all the visual and difficulty settings in ArmA, like friendly and enemy AI quality, HUD, crosshair, 3rd person view, clock indicator and so on.\
See [BiWiki: server.armaprofile#Arma_3][server armaprofile] and [BiWiki: Arma 3 Difficulty Menu][Difficulty Menu] for more info.
- Located at: `SERVER/<instance name>/serverconfig/server.Arma3Profile`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._

### servervars.Arma3Profile
Binary data maintained by the instance.\
The file is normally called `<instance name>.vars.Arma3Profile` in the profile dir, we just make a symlink from it to this file inside the `serverconfig` sub-directory of the instance to keep things more organized per instance.
- Located at: `SERVER/<instance name>/serverconfig/servervars.Arma3Profile`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._

### kick.txt & ban.txt
See: https://dev.arma3.com/post/spotrep-00088
- Located at: `SERVER/<instance name>/serverconfig/kick.txt`[^footnote-aidreader] from install directory.
- Located at: `SERVER/<instance name>/serverconfig/ban.txt`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._


## Scripts

### updateDS
This script is used to update the common files of the <abbr title="Linux Dedicated Game Server">LDGS</abbr>.\
Eg. the _Arma 3_ server itself plus default mods.
- Creates a similar named logfile in the log directory of what it did at last invocation.
- Usage: `bin/updateDS` from install directory.\
  _(Takes no arguments)_

### createInstance
This script is used to create a fresh instance from templates, which uses common files shared among other instances, to run the <abbr title="Linux Dedicated Game Server">LDGS</abbr>.
This allows you to setup separate <abbr title="Linux Dedicated Game Server">LDGS</abbr>'s with their own config.\
A "Test" instance will be automatically created using the `1stTimeInit` script in **step 3** of _[the Install instructions](#install)_.
- Creates a similar named logfile in the log directory of what it did at last invocation.
- Usage: `bin/createInstance <instance name> [<portdelta> [<serverip>]]`[^footnote-aidreader] from install directory.
  1. _\<instance name>_ **(Required) (case-sensitive)**\
      Defaults to `Test`.\
      The name of the instance.
  2. _\<portdelta>_ **(Optional)** but Required when next argument is to be used.\
      Defaults to `0`.\
      An integer to offset the port used to run the server on.\
      The final port used is calculated using:
      ```math
      \color{green}
      final port =
      f(portdelta) =
      2302 + 10 \times portdelta
      ```
  3. _\<serverip>_ **(Optional)**\
      Defaults to `127.0.0.1`.\
      The **IP#** that the <abbr title="Linux Dedicated Game Server">LDGS</abbr>-instance will listen on for connections.

### startserver
This script will only be created if your operating system is **NOT** using the [systemd][Systemd site] System and Service Manager.\
It can be used to start the instance.
- Usage: `SERVER/<instance name>/startserver <extra parameters>`[^footnote-aidreader] from install directory.\
  _Where `<instance name>` is same as used when [createInstance](#createInstance) was invoked._
  1. _\<extra parameters>_ **(Optional)**\
      Any extra parameters you want to use to start the server.

### startAndTailInstance
This script is only usable if your operating system is using the [systemd][Systemd site] System and Service Manager.\
It can be used while changing configurations of an instance.\
It will start the instance and show the stdout of the server in the console, and stop the instance when you press `CTRL+C`.
- Usage: `bin/startAndTailInstance <instance name>`[^footnote-aidreader] from install directory.
  1. _\<instance name>_ **(Required) (case-sensitive)**\
      The name of the instance, same as used when [createInstance](#createInstance) was invoked.

---
# References
[Company site]: https://www.bohemia.net/ "Visit Bohemia Interactive"
[Company logo]: images/Bohemia_Interactive_black.svg
[Steam page]: https://store.steampowered.com/app/107410/Arma_3/ "The steam store page for Arma3"
[Game site]: https://arma3.com/ "Visit the official game website"
[Game logo]: images/arma3_black.png
[SteamCMD Info]: https://developer.valvesoftware.com/wiki/SteamCMD#Manually "Visit Valve's wiki-page"
[SteamCMD 32bit libs on 64bit]: https://developer.valvesoftware.com/wiki/SteamCMD#32-bit_libraries_on_64-bit_Linux_systems "Visit Valve's wiki-page"
[Arma 3 Startup Parameters]: https://community.bistudio.com/wiki/Arma_3_Startup_Parameters "Visit Arma3's community wiki-page"
[basic cfg]: https://community.bistudio.com/wiki/basic.cfg "Visit Arma3's community wiki-page"
[server cfg]: https://community.bistudio.com/wiki/server.cfg "Visit Arma3's community wiki-page"
[server armaprofile]: https://community.bistudio.com/wiki/server.armaprofile#Arma_3  "Visit Arma3's community wiki-page"
[Difficulty Menu]: https://community.bistudio.com/wiki/Arma_3_Difficulty_Menu "Visit Arma3's community wiki-page"
[Systemd site]: https://www.freedesktop.org/wiki/Software/systemd/ "Visit the official Systemd wiki"

# Footnotes
[^footnote-aidreader]: The `[`,`]`,`<`,`>` characters shown are not to be typed _literaly_ as they are only to visually aid the reader (you)...
